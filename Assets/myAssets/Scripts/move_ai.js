#pragma strict

var moveSpeed: float;
var moveRight: boolean;

var wallCheck: Transform;
var wallCheckRadius: float;
var whatIsWall: LayerMask;
var hittingWall: boolean;

var atEdge: boolean;
var edgeCheck: Transform;

function Start () {


}

function Update () {
	hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
	atEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);

	if(hittingWall || !atEdge)
		moveRight = !moveRight;

	if(moveRight)
	{
		transform.localScale.x = -Mathf.Abs(transform.localScale.x);
		gameObject.GetComponent(Rigidbody2D).velocity = new Vector2(moveSpeed, gameObject.GetComponent(Rigidbody2D).velocity.y);
	}
	else 
	{
		transform.localScale.x = Mathf.Abs(transform.localScale.x);
		gameObject.GetComponent(Rigidbody2D).velocity = new Vector2(-moveSpeed, gameObject.GetComponent(Rigidbody2D).velocity.y);
	}
}
