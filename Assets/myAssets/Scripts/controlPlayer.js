﻿#pragma strict

var speed = .2;
var fluct = 0.05;
var y_normal = 0;
var min = 0;
var max = 0;
var crap:Rigidbody2D;

function fire_crap(){
	var crap_clone = Instantiate(crap, transform.position, transform.rotation);
	//var crap_clone = Instantiate(crap, transform.position, transform.rotation);
	var craplife = 10.0f;
	Destroy(crap_clone, craplife); // delete crap after 10 seconds no matter what
}

function Start () {
	y_normal = transform.position.y;
	min = y_normal - .5;
	max = y_normal + .5;
}

function Update(){
	var move_horizontal = Input.GetAxis('Horizontal');
	
	if(move_horizontal != 0)
		transform.localScale.x = Mathf.Sign(move_horizontal) * Mathf.Abs(transform.localScale.x);
	
	transform.position.x += move_horizontal * speed;
	transform.position.y = Mathf.PingPong(Time.time*2,max-min)+min;
	
	if (Input.GetKeyUp(KeyCode.Space)){
		fire_crap();
	}
}