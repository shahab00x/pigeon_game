#pragma strict

var speed;
var rd : Rigidbody2D;

function Start () {
	rd = GetComponent.<Rigidbody2D>();
}

function Update () {
	rd.AddForce(new Vector2(0, -40));
}

function OnTriggerEnter2D(other: Collider2D)
{
	if (other.gameObject.tag == "AI")
		ScoreManager.AddPoints(1);
	Destroy(gameObject);
}