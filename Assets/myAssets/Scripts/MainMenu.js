﻿#pragma strict

//function Start () {

//}

//function Update () {

//}

public var startLevel: String;
public var levelSelect: String;

public function NewGame()
{
	Application.LoadLevel(startLevel);
}

public function LevelSelect()
{
	Application.LoadLevel(levelSelect);
}

public function Quit()
{
	Debug.Log("Game Exited");
	Application.Quit();
}
