﻿#pragma strict
import UnityEngine.UI;

static var score:int;

function Start () {
	score = 0;
}

function Update () {
	if (score < 0)
		score = 0;

	GetComponent.<Text>().text = "" + score;
}

static function AddPoints(pointsToAdd:int)
{
	score += pointsToAdd;
}

static function Reset()
{
	score = 0;
}